// Initialize Amazon Cognito
const poolData = {
    UserPoolId: 'us-east-1_ZZVICaziX',
    ClientId: '10eacv8s4lppiur6hf6342qtvi'
};

const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

// Registration Form
document.getElementById('register-form').addEventListener('submit', function (e) {
    e.preventDefault();
    const username = document.getElementById('register-username').value;
    const email = document.getElementById('register-email').value;
    const password = document.getElementById('register-password').value;

    const attributeList = [
        new AmazonCognitoIdentity.CognitoUserAttribute({ Name: 'email', Value: email }),
    ];

    userPool.signUp(username, password, attributeList, null, function (err, result) {
        if (err) {
            console.error(err);
            alert("Registration failed: " + err.message);
        } else {
            const cognitoUser = result.user;
            console.log("Registration successful. User name is " + cognitoUser.getUsername());
            alert("Registration successful! Check your email for a verification code.");
        }
    });
});

// Login Form
document.getElementById('login-form').addEventListener('submit', function (e) {
    e.preventDefault();
    const username = document.getElementById('login-username').value;
    const password = document.getElementById('login-password').value;

    const authenticationData = {
        Username: username,
        Password: password,
    };

    const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);

    const userData = {
        Username: username,
        Pool: userPool,
    };

    const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (session) {
            console.log("Authentication successful");
            updateStatus('Logged in');
        },
        onFailure: function (err) {
            console.error(err);
            alert("Login failed: " + err.message);
        },
    });
});

function updateStatus(message) {
    document.getElementById('status-message').textContent = message;
}